# Systemd script for launch OpenAuto (AndroidAuto emulator)
 Tested only on ArchLinux

## Require
 - aur/openauto-git
 - systemd
 - openbox
 - xinit

## Install 
 - Git clone & copy all file in write path.
 - chmod +x /usr/sbin/autoapp-session

## Usage
### Display Manager ->
 - Select OpenAuto (Android Auto) in Desktop list.

### Systemd ->
 - systemctl start openauto.service

### Grub2 -> 
 - In linux line add : systemd.unit=openauto.target

## Thank
 - f1xpl (https://github.com/f1xpl)
 - Google
 - ArchLinux